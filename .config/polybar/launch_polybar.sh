#!/bin/bash
killall -q polybar

while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

exec polybar example 2>&1 & disown

echo "polybar launched"
