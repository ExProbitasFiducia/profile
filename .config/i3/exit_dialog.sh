#!/bin/bash
#if zenity --question --title="Log Out" --text="Log out?" --width=300 --height=300 ; then
#    i3-msg exec exit
#fi
timeout=3
for (( i=0 ; i <= $timeout ; i++ )) do 
    echo "# Shutdown in $[ $timeout - $i ]..."
    echo $[ 100 * $i / $timeout ]
    sleep 1
done | zenity  --progress --title="Shutting down automatically..."  \
    --window-icon=warning --width=400 --auto-close

if [ $? = 0 ] ; then
    i3-msg exit
fi
